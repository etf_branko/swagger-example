package com.bjovic.swagger.example.module;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;

import javax.servlet.ServletContextEvent;

public class SwaggerExampleContextListener extends GuiceServletContextListener {

    private GuiceResteasyBootstrapServletContextListener guiceResteasyBootstrapServletContextListener;

    @Override
    public Injector getInjector() {
        Injector injector = Guice.createInjector(new SwaggerExampleModule());
        guiceResteasyBootstrapServletContextListener = injector.getInstance(GuiceResteasyBootstrapServletContextListener.class);
        return injector;
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        super.contextInitialized(servletContextEvent);
        guiceResteasyBootstrapServletContextListener.contextInitialized(servletContextEvent);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        super.contextDestroyed(servletContextEvent);
        guiceResteasyBootstrapServletContextListener.contextDestroyed(servletContextEvent);
    }

}
