package com.bjovic.swagger.example.module;

import com.bjovic.swagger.example.rest.SwaggerService;
import com.google.inject.Binder;
import com.google.inject.Module;

public class SwaggerExampleModule implements Module {
    public void configure(Binder binder) {
        binder.bind(SwaggerService.class);
    }
}
