package com.bjovic.swagger.example.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/api")
public class SwaggerService {

    private static final String HELLO = "Hello";
    private static final String BYE = "Bye";

    @GET
    @Path("/hello")
    public Response hello() {
        return Response.status(Response.Status.OK).entity(HELLO).build();
    }

    @GET
    @Path("/bye")
    public Response bye() {
        return Response.status(Response.Status.OK).entity(BYE).build();
    }

}
